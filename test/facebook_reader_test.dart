// ignore_for_file: avoid_print

import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:kyanite/src/facebook/services/facebook_file_reader.dart';
import 'package:logging/logging.dart';

void main() {
  const String rootPath = 'test_assets';

  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    print(
        '${event.level.name} - ${event.loggerName} @ ${event.time}: ${event.message}');
  });

  // group('Test Facebook Reading timing', () {
  //   test('Normal Read vs. proper encoded read', () async {
  //     final expected = [
  //       'This is malformed and should be Polish diacritical character ą.',
  //       'This should be a heart ❤.',
  //       'This should be a five stars ★★ ★★ ★.',
  //     ];
  //     const path = '$rootPath/mangled.txt';
  //     final result = await File(path).readFacebookEncodedFileAsString();
  //     expect(result.isSuccess, true);
  //     final lines = result.get().split('\n');
  //     expect(lines.length, equals(expected.length));
  //     for(var i = 0; i < lines.length; i++) {
  //       //expect(lines[i], equals(expected[i]));
  //       print('|${lines[i]}| ?= |${expected[i]}|');
  //     }
  //   });

  group('Test Facebook Reading', () {
    test('Read encoded text from disk', () async {
      final expected = [
        'This is malformed and should be Polish diacritical character ą.',
        'This should be a heart ❤.',
        'This should be a five stars ★★ ★★ ★.',
      ];
      const path = '$rootPath/mangled.txt';
      final result = await File(path).readFacebookEncodedFileAsString();
      expect(result.isSuccess, true);
      final lines = result.value.split('\n');
      lines.forEach(print);
      expect(lines.length, equals(expected.length));
      for (var i = 0; i < lines.length; i++) {
        //expect(lines[i], equals(expected[i]));
        print('|${lines[i]}| ?= |${expected[i]}|');
      }
    });
  });
}
