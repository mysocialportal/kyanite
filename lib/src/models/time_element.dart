class TimeElement {
  final DateTime timestamp;
  final bool hasImages;
  final bool hasVideos;
  final String text;
  final String title;

  TimeElement(
      {int timeInMS = 0,
      this.hasImages = false,
      this.hasVideos = false,
      this.text = '',
      this.title = ''})
      : timestamp = DateTime.fromMillisecondsSinceEpoch(timeInMS);

  bool hasText(String phrase) =>
      text.contains(phrase) || title.contains(phrase);
}
