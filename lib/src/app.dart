import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/themes.dart';
import 'package:kyanite/src/utils/scrolling_behavior.dart';
import 'package:provider/provider.dart';

import 'facebook/services/path_mapping_service.dart';
import 'home.dart';
import 'settings/settings_controller.dart';

/// The Widget that configures your application.
class Kyanite extends StatelessWidget {
  static const minAppSize = Size(915, 700);

  const Kyanite({
    Key? key,
    required this.settingsController,
  }) : super(key: key);

  final SettingsController settingsController;

  @override
  Widget build(BuildContext context) {
    DesktopWindow.setMinWindowSize(minAppSize);
    final pathMappingService = PathMappingService(settingsController);
    final archiveService = FacebookArchiveDataService(
        pathMappingService: pathMappingService,
        appDataDirectory: settingsController.appDataDirectory.path);
    settingsController.addListener(() {
      archiveService.clearCaches();
      pathMappingService.refresh();
    });
    return AnimatedBuilder(
      animation: settingsController,
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          restorationScopeId: 'app',
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en', ''), // English, no country code
          ],
          onGenerateTitle: (BuildContext context) =>
              AppLocalizations.of(context)!.appTitle,
          theme: KyaniteTheme.light,
          darkTheme: KyaniteTheme.dark,
          themeMode: settingsController.themeMode,
          scrollBehavior: FacebookAppScrollingBehavior(),
          home: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (context) => settingsController),
              ChangeNotifierProvider(create: (context) => archiveService),
              Provider(create: (context) => pathMappingService),
            ],
            child: Home(settingsController: settingsController),
          ),
        );
      },
    );
  }
}
