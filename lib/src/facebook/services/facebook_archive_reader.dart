import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_album.dart';
import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/models/facebook_event.dart';
import 'package:kyanite/src/facebook/models/facebook_friend.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_conversation.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/facebook_saved_item.dart';
import 'package:kyanite/src/facebook/models/facebook_timeline_type.dart';
import 'package:kyanite/src/facebook/services/facebook_file_reader.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../../utils/temp_file_builder.dart';

class FacebookArchiveFolderReader extends ChangeNotifier {
  static final _logger = Logger('$FacebookArchiveFolderReader');
  static final expectedDirectories = [
    'posts',
    'comments_and_reactions',
    'saved_items_and_collections',
    'posts/media',
    'posts/album',
    'events',
    'messages',
  ];

  String _rootDirectoryPath = '';

  String get rootDirectoryPath => _rootDirectoryPath;

  set rootDirectoryPath(String value) {
    _rootDirectoryPath = value;
    notifyListeners();
  }

  FacebookArchiveFolderReader(String rootDirectoryPath) {
    _rootDirectoryPath = rootDirectoryPath;
    _logger.fine('Create new FacebookArchiveFolderReader');
  }

  FutureResult<List<FacebookPost>, ExecError> readPosts() async {
    final posts = <FacebookPost>[];
    final errors = <ExecError>[];

    final yourPostPath = '$rootDirectoryPath/posts/your_posts_1.json';
    if (File(yourPostPath).existsSync()) {
      (await _getJsonList(yourPostPath))
          .andThen(
              (json) => _parsePostResults(json, FacebookTimelineType.active))
          .match(
              onSuccess: (newPosts) => posts.addAll(newPosts),
              onError: (error) {
                _logger
                    .severe('Error $error responses json for ${yourPostPath}');
                errors.add(error);
              });
    }

    final archivedPostsPath = '$rootDirectoryPath/posts/archive.json';
    if (File(archivedPostsPath).existsSync()) {
      (await _getJson(archivedPostsPath))
          .andThen((json) => json.containsKey('archive_v2')
              ? Result.ok(json['archive_v2'])
              : Result.error(
                  ExecError.message('No archive_v2 key in $archivedPostsPath')))
          .andThen((archivedPostsJson) => _parsePostResults(
              archivedPostsJson, FacebookTimelineType.archive))
          .match(
              onSuccess: (archivedPosts) => posts.addAll(archivedPosts),
              onError: (error) {
                _logger.severe(
                    'Error $error responses json for $archivedPostsPath');
                errors.add(error);
              });
    }

    final trashPostsPath = '$rootDirectoryPath/posts/trash.json';
    if (File(trashPostsPath).existsSync()) {
      (await _getJson(trashPostsPath))
          .andThen((json) => json.containsKey('trash_v2')
              ? Result.ok(json['trash_v2'])
              : Result.error(
                  ExecError.message('No trash_v2 key in $trashPostsPath')))
          .andThen((archivedPostsJson) =>
              _parsePostResults(archivedPostsJson, FacebookTimelineType.trash))
          .match(
              onSuccess: (archivedPosts) => posts.addAll(archivedPosts),
              onError: (error) {
                _logger
                    .severe('Error $error responses json for $trashPostsPath');
                errors.add(error);
              });
    }

    if (errors.isNotEmpty) {
      return Result.error(ExecError.message(
          'Error reading one or more present post files. Check logs for more details.'));
    }

    return Result.ok(posts);
  }

  FutureResult<List<FacebookComment>, ExecError> readComments() async {
    final path = '$rootDirectoryPath/comments_and_reactions/comments.json';
    final jsonResult = await _getJson(path);
    if (jsonResult.isFailure) {
      return Result.error(jsonResult.error);
    }

    final jsonData = jsonResult.value;
    if (!jsonData.containsKey('comments_v2')) {
      return Result.error(
          ExecError(errorMessage: 'Comments JSON file is malformed: $path'));
    }

    final commentsJson = jsonData['comments_v2'] as List<dynamic>;
    final commentsResult = runCatching(() => Result.ok(
        commentsJson.map((e) => FacebookComment.fromFacebookJson(e)).toList()));

    commentsResult.match(
        onSuccess: (value) => _logger.fine('Comments processed into PODOs'),
        onError: (error) =>
            _logger.severe('Error mapping JSON to post data: $error'));

    return commentsResult.mapExceptionErrorToExecError();
  }

  FutureResult<List<FacebookAlbum>, ExecError> readPhotoAlbums() async {
    final albumFolderPath = '$rootDirectoryPath/posts/album';
    final folder = Directory(albumFolderPath);
    final albums = <FacebookAlbum>[];

    if (!folder.existsSync()) {
      final msg = 'Photos folder does not exist; $albumFolderPath';
      _logger.severe(msg);
      return Result.error(ExecError(errorMessage: msg));
    }

    await for (var entity in folder.list(recursive: true)) {
      final filePath = entity.path;
      if (entity.statSync().type != FileSystemEntityType.file) {
        _logger
            .severe("Unexpected file/folder in photo albums folder: $filePath");
        continue;
      }

      if (!entity.path.toLowerCase().endsWith('json')) {
        _logger
            .severe("Unexpected file type in photo albums folder: $filePath");
        continue;
      }

      final jsonResult = await _getJson(filePath);
      jsonResult.match(
          onSuccess: (json) {
            final albumResult =
                runCatching(() => Result.ok(FacebookAlbum.fromJson(json)));
            albumResult.match(
                onSuccess: (album) {
                  albums.add(album);
                  _logger.fine('Album converted to PODO');
                },
                onError: (error) =>
                    _logger.severe('Error parsing album JSON for $filePath'));
          },
          onError: (error) =>
              _logger.severe('Error parsing photo album: $filePath'));
    }

    return Result.ok(albums);
  }

  FutureResult<List<FacebookFriend>, ExecError> readFriends() async {
    final basePath = '$rootDirectoryPath/friends_and_followers';
    final friendsFile = File('$basePath/friends.json');
    final receivedFile = File('$basePath/friend_requests_received.json');
    final rejectedFile = File('$basePath/rejected_friend_requests.json');
    final removedFile = File('$basePath/removed_friends.json');
    final sentFile = File('$basePath/friend_requests_sent.json');
    final allFriends = <FacebookFriend>[];

    if (!Directory(basePath).existsSync()) {
      _logger.severe('Friends base folder does not exist: $basePath');
      return Result.error(
          ExecError(errorMessage: 'Friends data does not exist'));
    }

    (await _readFriendsJsonFile(
            friendsFile, FriendStatus.friends, "friends_v2"))
        .match(
            onSuccess: (friends) => allFriends.addAll(friends),
            onError: (error) => _logger.info(
                "Errors processing friends.json, continuing on without that data"));

    (await _readFriendsJsonFile(
            receivedFile, FriendStatus.requestReceived, "received_requests_v2"))
        .match(
            onSuccess: (friends) => allFriends.addAll(friends),
            onError: (error) => _logger.info(
                "Errors processing received_friend_requests.json, continuing on without that data"));

    (await _readFriendsJsonFile(
            rejectedFile, FriendStatus.rejectedRequest, "rejected_requests_v2"))
        .match(
            onSuccess: (friends) => allFriends.addAll(friends),
            onError: (error) => _logger.info(
                "Errors processing rejected_friend_requests.json, continuing on without that data"));

    (await _readFriendsJsonFile(
            removedFile, FriendStatus.removed, "deleted_friends_v2"))
        .match(
            onSuccess: (friends) => allFriends.addAll(friends),
            onError: (error) => _logger.info(
                "Errors processing removed_friends.json, continuing on without that data"));

    (await _readFriendsJsonFile(
            sentFile, FriendStatus.removed, "sent_requests_v2"))
        .match(
            onSuccess: (friends) => allFriends.addAll(friends),
            onError: (error) => _logger.info(
                "Errors processing sent_friend_requests.json, continuing on without that data"));

    return Result.ok(allFriends);
  }

  FutureResult<List<FacebookEvent>, ExecError> readEvents() async {
    final basePath = '$rootDirectoryPath/events';
    final invitationsFile = File('$basePath/event_invitations.json');
    final responsesFile = File('$basePath/your_event_responses.json');
    final yourEventsFile = File('$basePath/your_events.json');
    final events = <FacebookEvent>[];

    if (!Directory(basePath).existsSync()) {
      _logger.severe('Events base folder does not exist: $basePath');
      return Result.error(
          ExecError(errorMessage: 'Events data does not exist'));
    }

    if (invitationsFile.existsSync()) {
      final json = (await _getJson(invitationsFile.path)).fold(
          onSuccess: (json) => json,
          onError: (error) {
            _logger.severe(
                'Error $error reading json for ${invitationsFile.path}');
            return <String, dynamic>{};
          });
      final List<dynamic> invited =
          json['events_invited_v2'] ?? <Map<String, dynamic>>[];
      try {
        events.addAll(invited.map((e) => FacebookEvent.fromJson(e,
            statusType: FacebookEventStatus.invited)));
      } catch (e) {
        _logger.severe(
            'Error $e processing JSON invitations file: ${invitationsFile.path}');
      }
    } else {
      _logger.info('Invitations file does not exist; ${invitationsFile.path}');
    }

    if (responsesFile.existsSync()) {
      final json = (await _getJson(responsesFile.path)).fold(
          onSuccess: (json) => json,
          onError: (error) {
            _logger.severe(
                'Error $error responses json for ${responsesFile.path}');
            return <String, dynamic>{};
          });
      final Map<String, dynamic> responses =
          json['event_responses_v2'] ?? <String, dynamic>{};
      final List<dynamic> joined = responses['events_joined'] ?? [];
      try {
        events.addAll(joined.map((e) =>
            FacebookEvent.fromJson(e, statusType: FacebookEventStatus.joined)));
      } catch (e) {
        _logger.severe(
            'Error $e processing JSON joined events file: ${invitationsFile.path}');
      }
      final List<dynamic> declined = responses['events_declined'] ?? [];
      try {
        events.addAll(declined.map((e) => FacebookEvent.fromJson(e,
            statusType: FacebookEventStatus.declined)));
      } catch (e) {
        _logger.severe(
            'Error $e processing JSON declined events file: ${invitationsFile.path}');
      }
      final List<dynamic> interested = responses['events_interested'] ?? [];
      try {
        events.addAll(interested.map((e) => FacebookEvent.fromJson(e,
            statusType: FacebookEventStatus.declined)));
      } catch (e) {
        _logger.severe(
            'Error $e processing JSON interested events file: ${invitationsFile.path}');
      }
    } else {
      _logger.info('Responses file does not exist; ${responsesFile.path}');
    }

    if (yourEventsFile.existsSync()) {
      final json = (await _getJson(yourEventsFile.path)).fold(
          onSuccess: (json) => json,
          onError: (error) {
            _logger.severe(
                'Error $error your events file json for ${responsesFile.path}');
            return <String, dynamic>{};
          });
      final List<dynamic> yourEvents =
          json['your_events_v2'] ?? <Map<String, dynamic>>[];
      try {
        events.addAll(yourEvents.map((e) =>
            FacebookEvent.fromJson(e, statusType: FacebookEventStatus.owner)));
      } catch (e) {
        _logger.severe(
            'Error $e processing JSON your events file: ${yourEventsFile.path}');
      }
    } else {
      _logger.info('Your events file does not exist ${yourEventsFile.path}');
    }

    events.sort((e1, e2) => -e1.startTimestamp.compareTo(e2.startTimestamp));

    return Result.ok(events);
  }

  FutureResult<List<FacebookMessengerConversation>, ExecError>
      readConversations() async {
    final path = '$rootDirectoryPath/messages';
    final folder = Directory(path);
    final conversations = <String, FacebookMessengerConversation>{};

    if (!folder.existsSync()) {
      _logger.severe('Messages folder does not exist; $path');
      return Result.ok([]);
    }

    await for (var entity in folder.list(recursive: true)) {
      if (entity.path.toLowerCase().endsWith('json')) {
        if (entity is Directory) {
          continue;
        }

        try {
          final jsonResult = await _getJson(entity.path, level: Level.FINEST);
          if (jsonResult.isFailure) {
            _logger.severe(
                'Error ${jsonResult.error} reading JSON data for ${entity.path}');
            continue;
          }

          final conversation =
              FacebookMessengerConversation.fromFacebookJson(jsonResult.value);
          if (conversations.containsKey(conversation.id)) {
            final existingConvo = conversations[conversation.id]!;
            existingConvo.messages.addAll(conversation.messages);
            existingConvo.messages
                .sort((m1, m2) => -m1.timestampMS.compareTo(m2.timestampMS));
          } else {
            conversations[conversation.id] = conversation;
          }
        } catch (e) {
          _logger.severe('Error $e processing conversation ${entity.path}');
        }
      }
    }

    return Result.ok(conversations.values.toList());
  }

  FutureResult<List<FacebookSavedItem>, ExecError> readSavedItems() async {
    final path =
        '$rootDirectoryPath/saved_items_and_collections/saved_items_and_collections.json';
    final jsonResult = await _getJson(path);
    if (jsonResult.isFailure) {
      return Result.error(jsonResult.error);
    }

    final jsonData = jsonResult.value;
    if (!jsonData.containsKey('saves_and_collections_v2')) {
      return Result.error(ExecError(
          errorMessage:
              'Saved Items and Collections JSON file is malformed: $path'));
    }

    final savedItemsJson =
        jsonData['saves_and_collections_v2'] as List<dynamic>;
    final savedItemsResult = runCatching(() => Result.ok(savedItemsJson
        .map((e) => FacebookSavedItem.fromFacebookJson(e))
        .toList()));

    savedItemsResult
        .andThen(
            (items) => Result.ok(items.where((e) => e.timestamp != 0).toList()))
        .match(
            onSuccess: (value) =>
                _logger.fine('Saved Items processed into PODOs'),
            onError: (error) => _logger
                .severe('Error mapping JSON to saved items data: $error'));

    return savedItemsResult.mapExceptionErrorToExecError();
  }

  static bool validateCanReadArchive(String path) {
    _logger.fine('Validating whether path is a valid Facebook Archive: $path');
    final baseDir = Directory(path);
    if (!baseDir.existsSync()) {
      _logger.severe('Unable to find base directory: $path');
      return false;
    }

    try {
      baseDir.listSync();
    } catch (e) {
      _logger.severe('Unable to access base directory: $path');
      return false;
    }

    return true;
  }

  Result<List<FacebookPost>, ExecError> _parsePostResults(
      List<dynamic> json, FacebookTimelineType timelineType) {
    final postsResult = runCatching(() => Result.ok(
        json.map((e) => FacebookPost.fromJson(e, timelineType)).toList()));

    postsResult.match(
        onSuccess: (value) => _logger.fine('Posts processed into PODOs'),
        onError: (error) =>
            _logger.severe('Error mapping JSON to post data: $error'));
    return postsResult.mapError((error) =>
        error is ExecError ? error : ExecError.message(error.toString()));
  }

  static FutureResult<Map<String, dynamic>, ExecError> _getJson(String path,
      {Level level = Level.FINE}) async {
    final file = File(path);
    final result = await (await _readFacebookFile(file, level)).andThenAsync(
        (jsonText) async => await _parseJsonFileText<Map<String, dynamic>>(
            jsonText, file, level));
    return result.mapError((error) => error as ExecError);
  }

  static FutureResult<List<dynamic>, ExecError> _getJsonList(String path,
      {Level level = Level.FINE}) async {
    final file = File(path);
    final fileTextResponse = await _readFacebookFile(file, level);
    if (fileTextResponse.isFailure) {
      return Result.error(fileTextResponse.error);
    }

    final jsonText = fileTextResponse.value.trim();
    if (!jsonText.startsWith('[')) {
      final parsedJsonResult =
          await _parseJsonFileText<Map<String, dynamic>>(jsonText, file, level);
      return parsedJsonResult.mapValue((value) => [value]);
    }
    return await _parseJsonFileText<List<dynamic>>(jsonText, file, level);
  }

  static FutureResult<String, ExecError> _readFacebookFile(
      File file, Level level) async {
    _logger.log(level, 'Attempting to open and read ${file.path}');
    final response = await file.readFacebookEncodedFileAsString();
    response.match(
        onSuccess: (value) => _logger.log(level, 'Text read from ${file.path}'),
        onError: (error) async {
          final tmpPath =
              await getTempFile(file.uri.pathSegments.last, '.fragment.json');
          await File(tmpPath).writeAsString(response.error.errorMessage);
          _logger.severe('Wrote partial read of ${file.path} to $tmpPath');
        });

    return response;
  }

  static FutureResult<T, ExecError> _parseJsonFileText<T>(
      String text, File originalFile, Level levelForFullDump) async {
    final jsonParseResult = runCatching(() => Result.ok(jsonDecode(text) as T))
        .mapExceptionErrorToExecError();
    final msg = jsonParseResult.fold(
        onSuccess: (value) => 'JSON decoded from ${originalFile.path}',
        onError: (error) async {
          final tmpPath = await getTempFile(
              originalFile.uri.pathSegments.last, '.ingested.json');
          await File(tmpPath).writeAsString(text);
          _logger.severe(
              'Wrote ingested JSON stream text read of ${originalFile.path} to $tmpPath');

          return 'Error parsing json for ${originalFile.path}';
        });
    _logger.log(levelForFullDump, msg);
    return jsonParseResult;
  }

  FutureResult<List<FacebookFriend>, ExecError> _readFriendsJsonFile(
      File file, FriendStatus status, String topKey) async {
    final friends = <FacebookFriend>[];

    if (file.existsSync()) {
      final json = (await _getJson(file.path)).fold(
          onSuccess: (json) => json,
          onError: (error) {
            _logger.severe('Error $error reading json for ${file.path}');
            return <String, dynamic>{};
          });
      final List<dynamic> invited = json[topKey] ?? <Map<String, dynamic>>[];
      try {
        final entries = invited.map((f) => FacebookFriend.fromJson(f, status));
        _logger.fine(
            '${entries.length} friends of type $status found in ${file.path}');
        friends.addAll(entries);
      } catch (e) {
        _logger.severe('Error $e processing JSON $topKey file: ${file.path}');
      }
    } else {
      _logger.info('$topKey file does not exist; ${file.path}');
    }

    return Result.ok(friends);
  }
}
