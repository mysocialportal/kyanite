import 'dart:io';

import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;

class PathMappingService {
  static final _logger = Logger('$PathMappingService');
  final SettingsController settings;
  final _archiveDirectories = <FileSystemEntity>[];

  PathMappingService(this.settings) {
    refresh();
  }

  String get rootFolder => settings.rootFolder;

  List<FileSystemEntity> get archiveDirectories =>
      List.unmodifiable(_archiveDirectories);

  void refresh() {
    _logger.fine('Refreshing path mapping service directory data.');
    if (!Directory(settings.rootFolder).existsSync()) {
      _logger.severe(
          "Base directory does not exist! can't do mapping of ${settings.rootFolder}");
      return;
    }
    _archiveDirectories.clear();

    try {
      if (_calcRootIsSingleArchiveFolder()) {
        _archiveDirectories.add(Directory(rootFolder));
        return;
      }
    } catch (e) {
      _logger
          .severe('Error thrown while trying to calculate root structure: $e');
      return;
    }

    _archiveDirectories.addAll(Directory(settings.rootFolder)
        .listSync(recursive: false)
        .where((element) =>
            element.statSync().type == FileSystemEntityType.directory));
  }

  String toFullPath(String relPath) {
    for (final file in _archiveDirectories) {
      final fullPath = p.join(file.path, relPath);
      if (File(fullPath).existsSync()) {
        return fullPath;
      }
    }

    _logger.fine(
        'Did not find a file with this relPath anywhere therefore returning the relPath');
    return relPath;
  }

  bool _calcRootIsSingleArchiveFolder() {
    for (final entity in Directory(rootFolder).listSync(recursive: false)) {
      if (_knownRootFilesAndFolders.contains(entity.uri.pathSegments
          .where((element) => element.isNotEmpty)
          .last)) {
        return true;
      }
    }

    return false;
  }

  static final _knownRootFilesAndFolders = [
    "facebook_100000044480872.zip.enc",
    "activity_messages",
    "ads_information",
    "apps_and_websites_off_of_facebook",
    "bug_bounty",
    "campus",
    "comments_and_reactions",
    "events",
    "facebook_accounts_center",
    "facebook_assistant",
    "facebook_gaming",
    "facebook_marketplace",
    "facebook_news",
    "facebook_payments",
    "friends_and_followers",
    "fundraisers",
    "groups",
    "journalist_registration",
    "live_audio_rooms",
    "location",
    "messages",
    "music_recommendations",
    "news_feed",
    "notifications",
    "other_activity",
    "other_logged_information",
    "other_personal_information",
    "pages",
    "polls",
    "posts",
    "preferences",
    "privacy_checkup",
    "profile_information",
    "reviews",
    "saved_items_and_collections",
    "search",
    "security_and_login_information",
    "shops_questions_&_answers",
    "short_videos",
    "soundbites",
    "stories",
    "volunteering",
    "voting_location_and_reminders",
    "your_interactions_on_facebook",
    "your_places",
    "your_problem_reports",
    "your_topics",
  ];
}
