import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/comment_card.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookCommentsScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookCommentsScreen');

  const FacebookCommentsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final service = Provider.of<FacebookArchiveDataService>(context);
    final username = Provider.of<SettingsController>(context).facebookName;

    _logger.fine('Build FacebookPostListView');

    return FutureBuilder<Result<List<FacebookComment>, ExecError>>(
        future: service.getComments(),
        builder: (context, snapshot) {
          _logger.fine('Future Comment builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading Comments');
          }

          final commentsResult = snapshot.requireData;
          if (commentsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting comments', error: commentsResult.error);
          }

          final comments = commentsResult.value;
          if (comments.isEmpty) {
            return const StandInStatusScreen(title: 'No comments were found');
          }
          _logger.fine('Build Comments ListView');
          return _FacebookCommentsScreenWidget(
              comments: comments, username: username);
        });
  }
}

class _FacebookCommentsScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookCommentsScreenWidget');
  final List<FacebookComment> comments;
  final String username;

  const _FacebookCommentsScreenWidget(
      {Key? key, required this.comments, required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    return FilterControl<FacebookComment, dynamic>(
        allItems: comments,
        imagesOnlyFilterFunction: (comment) => comment.hasImages(),
        videosOnlyFilterFunction: (comment) => comment.hasVideos(),
        textSearchFilterFunction: (comment, text) =>
            comment.title.contains(text) || comment.comment.contains(text),
        itemToDateTimeFunction: (comment) =>
            DateTime.fromMillisecondsSinceEpoch(
                comment.creationTimestamp * 1000),
        dateRangeFilterFunction: (comment, start, stop) =>
            timestampInRange(comment.creationTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No comments meet filter criteria');
          }

          return ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: ListView.separated(
                primary: false,
                restorationId: 'facebookCommentsListView',
                itemCount: items.length,
                itemBuilder: (context, index) {
                  _logger.finer('Rendering FacebookComment List Item');
                  final comment = items[index];
                  final newTitle = username.isEmpty
                      ? comment.title
                      : comment.title
                          .replaceAll(username, 'You')
                          .replaceAll(wholeWordRegEx('his'), 'your')
                          .replaceAll(wholeWordRegEx('her'), 'your');
                  final cardComment = username.isEmpty
                      ? comment
                      : comment.copy(title: newTitle);
                  return CommentCard(comment: cardComment);
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.black,
                    thickness: 0.2,
                  );
                }),
          );
        });
  }
}
