import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/components/post_card.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookVideosScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookVideosScreen');

  const FacebookVideosScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Build FacebookVideosScreen');
    final service = Provider.of<FacebookArchiveDataService>(context);

    return FutureBuilder<Result<List<FacebookPost>, ExecError>>(
        future: service.getPosts(),
        builder: (context, snapshot) {
          _logger.fine('FacebookVideosScreen Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading videos');
          }

          final result = snapshot.requireData;
          if (result.isFailure) {
            return ErrorScreen(
                title: 'Error getting video posts', error: result.error);
          }

          final videos = result.value
              .where((p) => p.mediaAttachments
                  .where((m) =>
                      m.estimatedType() == FacebookAttachmentMediaType.video)
                  .isNotEmpty)
              .toList();

          if (videos.isEmpty) {
            return const StandInStatusScreen(title: 'No videos were found');
          }

          _logger.fine('Build Videos ListView');
          return _FacebookVideosScreenWidget(posts: videos);
        });
  }
}

class _FacebookVideosScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookVideosScreenWidget');
  final List<FacebookPost> posts;

  const _FacebookVideosScreenWidget({Key? key, required this.posts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    return FilterControl<FacebookPost, dynamic>(
        allItems: posts,
        textSearchFilterFunction: (post, text) =>
            post.title.contains(text) || post.post.contains(text),
        itemToDateTimeFunction: (post) =>
            DateTime.fromMillisecondsSinceEpoch(post.creationTimestamp * 1000),
        dateRangeFilterFunction: (post, start, stop) =>
            timestampInRange(post.creationTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No videos meet filter criteria');
          }

          return ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: ListView.separated(
                primary: false,
                restorationId: 'facebookVideosListView',
                itemCount: items.length,
                itemBuilder: (context, index) {
                  _logger.finer('Rendering Facebook Video List Item');
                  return PostCard(post: items[index]);
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.black,
                    thickness: 0.2,
                  );
                }),
          );
        });
  }
}
