import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/facebook_media_wrapper_component.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_album.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/screens/facebook_photo_album_screen.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookPhotoAlbumsBrowserScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookPhotoAlbumsBrowserScreen');

  const FacebookPhotoAlbumsBrowserScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Build FacebookAlbumListView');
    final service = Provider.of<FacebookArchiveDataService>(context);

    return FutureBuilder<Result<List<FacebookAlbum>, ExecError>>(
        future: service.getAlbums(),
        builder: (futureBuilderContext, snapshot) {
          _logger.fine('FacebookAlbumListView Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading albums');
          }

          final albumsResult = snapshot.requireData;
          if (albumsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting comments', error: albumsResult.error);
          }

          final albums = albumsResult.value;

          if (albums.isEmpty) {
            return const StandInStatusScreen(title: 'No albums were found');
          }

          _logger.fine('Build Photo Albums Grid View');
          return _FacebookPhotoAlbumsBrowserScreenWidget(albums: albums);
        });
  }
}

class _FacebookPhotoAlbumsBrowserScreenWidget extends StatelessWidget {
  final List<FacebookAlbum> albums;

  const _FacebookPhotoAlbumsBrowserScreenWidget(
      {Key? key, required this.albums})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingsController = Provider.of<SettingsController>(context);
    final pathMapper = Provider.of<PathMappingService>(context);

    return FilterControl<FacebookAlbum, dynamic>(
        allItems: albums,
        textSearchFilterFunction: (album, text) =>
            album.name.contains(text) || album.description.contains(text),
        itemToDateTimeFunction: (album) => DateTime.fromMillisecondsSinceEpoch(
            album.lastModifiedTimestamp * 1000),
        dateRangeFilterFunction: (album, start, stop) =>
            timestampInRange(album.lastModifiedTimestamp * 1000, start, stop),
        builder: (context, albums) {
          return Padding(
            padding: const EdgeInsets.only(
              left: 16,
              right: 16,
              top: 16,
            ),
            child: GridView.builder(
              primary: false,
              itemCount: albums.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                mainAxisExtent: 255,
                maxCrossAxisExtent: 225,
              ),
              itemBuilder: (itemBuilderContext, index) {
                final album = albums[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (routeContext) {
                      return MultiProvider(providers: [
                        ChangeNotifierProvider.value(value: settingsController),
                        Provider.value(value: pathMapper)
                      ], child: FacebookPhotoAlbumScreen(album: album));
                    }));
                  },
                  child: SizedBox(
                      width: 200,
                      height: 200,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FacebookMediaWrapperComponent(
                                preferredWidth: 150,
                                preferredHeight: 150,
                                mediaAttachment: album.coverPhoto),
                            const SizedBox(height: 5),
                            Text(
                              '${album.name} ',
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(height: 5),
                            Text('(${album.photos.length} photos)'),
                          ])),
                );
              },
            ),
          );
        });
  }
}
