import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/facebook_link_elements_component.dart';
import 'package:kyanite/src/facebook/components/facebook_media_timeline_component.dart';
import 'package:kyanite/src/facebook/models/facebook_location_data.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/facebook_timeline_type.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/clipboard_helper.dart';
import 'package:provider/provider.dart';

class PostCard extends StatelessWidget {
  final FacebookPost post;

  const PostCard({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Scrollable.recommendDeferredLoadingForContext(context)) {
      return const SizedBox();
    }

    const double spacingHeight = 5.0;
    final formatter =
        Provider.of<SettingsController>(context).dateTimeFormatter;
    final mapper = Provider.of<PathMappingService>(context);

    final title = post.title.isEmpty ? 'Post' : post.title;
    final dateStamp = ' At ' +
        formatter.format(
            DateTime.fromMillisecondsSinceEpoch(post.creationTimestamp * 1000)
                .toLocal());

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Wrap(
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(dateStamp,
                    style: const TextStyle(
                      fontStyle: FontStyle.italic,
                    )),
                if (post.timelineType != FacebookTimelineType.active)
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Tooltip(
                        message:
                            'Post is in ${post.timelineType == FacebookTimelineType.trash ? 'Trash' : 'Archive'}',
                        child: Icon(
                          post.timelineType == FacebookTimelineType.trash
                              ? Icons.delete_outline
                              : Icons.archive_outlined,
                          color: Theme.of(context).disabledColor,
                        )),
                  ),
                Tooltip(
                  message: 'Copy text version of post to clipboard',
                  child: IconButton(
                      onPressed: () async => await copyToClipboard(
                          context: context,
                          text: post.toHumanString(mapper, formatter),
                          snackbarMessage: 'Copied Post to clipboard'),
                      icon: const Icon(Icons.copy)),
                ),
              ]),
          if (post.post.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            Text(post.post)
          ],
          if (post.locationData.hasData())
            post.locationData.toWidget(spacingHeight),
          if (post.links.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            FacebookLinkElementsComponent(links: post.links)
          ],
          if (post.mediaAttachments.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            FacebookMediaTimelineComponent(
                mediaAttachments: post.mediaAttachments)
          ]
        ],
      ),
    );
  }
}
