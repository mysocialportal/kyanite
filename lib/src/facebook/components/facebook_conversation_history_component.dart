import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_conversation.dart';
import 'package:kyanite/src/screens/standin_status_screen.dart';

class FacebookConversationHistoryComponent extends StatefulWidget {
  static final FacebookMessengerConversation noConversationSelected =
      FacebookMessengerConversation.empty();

  final FacebookMessengerConversation conversation;

  const FacebookConversationHistoryComponent(
      {Key? key, required this.conversation})
      : super(key: key);

  @override
  State<FacebookConversationHistoryComponent> createState() =>
      _FacebookConversationHistoryComponentState();
}

class _FacebookConversationHistoryComponentState
    extends State<FacebookConversationHistoryComponent> {
  @override
  Widget build(BuildContext context) {
    if (widget.conversation ==
        FacebookConversationHistoryComponent.noConversationSelected) {
      return const StandInStatusScreen(
        title: 'No conversation selected',
        subTitle: 'Select a conversation to display here',
      );
    }

    return ListView.separated(
        primary: false,
        restorationId: 'facebookConversationPane',
        itemCount: widget.conversation.messages.length,
        itemBuilder: (context, index) {
          final message = widget.conversation.messages[index];
          return Text(
            '${message.from}: ${message.message}',
            softWrap: true,
          );
        },
        separatorBuilder: (context, index) {
          return const Divider(
            color: Colors.black,
            thickness: 0.2,
          );
        });
  }
}
