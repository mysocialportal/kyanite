import 'dart:ui';

import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:latlng/latlng.dart';
import 'package:map/map.dart';

import 'marker_data.dart';

extension GeoSpatialPostExtensions on FacebookPost {
  MarkerData toMarkerData(MapTransformer transformer, Color color) {
    final latLon = LatLng(locationData.latitude, locationData.longitude);
    final offset = transformer.fromLatLngToXYCoords(latLon);
    return MarkerData(this, offset, color);
  }
}
