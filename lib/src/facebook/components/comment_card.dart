import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/clipboard_helper.dart';
import 'package:provider/provider.dart';

import 'facebook_link_elements_component.dart';
import 'facebook_media_timeline_component.dart';

class CommentCard extends StatelessWidget {
  final FacebookComment comment;

  const CommentCard({Key? key, required this.comment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Scrollable.recommendDeferredLoadingForContext(context)) {
      return const SizedBox();
    }

    const double spacingHeight = 5.0;
    final formatter = context.read<SettingsController>().dateTimeFormatter;
    final title = comment.title.isEmpty ? 'Comment' : comment.title;
    final mapper = Provider.of<PathMappingService>(context);
    final dateStamp = ' At ' +
        formatter.format(DateTime.fromMillisecondsSinceEpoch(
                comment.creationTimestamp * 1000)
            .toLocal());

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Wrap(
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(dateStamp,
                    style: const TextStyle(
                      fontStyle: FontStyle.italic,
                    )),
                Tooltip(
                  message: 'Copy text version of comment to clipboard',
                  child: IconButton(
                      onPressed: () async => await copyToClipboard(
                          context: context,
                          text: comment.toHumanString(mapper, formatter),
                          snackbarMessage: 'Copied Comment to clipboard'),
                      icon: const Icon(Icons.copy)),
                ),
              ]),
          if (comment.comment.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            Text(comment.comment)
          ],
          if (comment.links.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            FacebookLinkElementsComponent(links: comment.links)
          ],
          if (comment.mediaAttachments.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            FacebookMediaTimelineComponent(
                mediaAttachments: comment.mediaAttachments)
          ],
        ],
      ),
    );
  }
}
