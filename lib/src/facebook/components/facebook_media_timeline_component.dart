import 'dart:math';

import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/screens/facebook_media_slideshow_screen.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:provider/provider.dart';

import 'facebook_media_wrapper_component.dart';

class FacebookMediaTimelineComponent extends StatelessWidget {
  static const double _maxHeightWidth = 400.0;

  final List<FacebookMediaAttachment> mediaAttachments;

  const FacebookMediaTimelineComponent(
      {Key? key, required this.mediaAttachments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (mediaAttachments.isEmpty) {
      return const SizedBox(width: 0, height: 0);
    }

    final bool isSingle = mediaAttachments.length == 1;
    final double singleWidth = MediaQuery.of(context).size.width / 2.0;
    final double threeAcrossWidth = MediaQuery.of(context).size.width / 3.0;
    final double preferredMultiWidth = min(threeAcrossWidth, _maxHeightWidth);
    final pathMapper = Provider.of<PathMappingService>(context);
    final settingsController = Provider.of<SettingsController>(context);

    return Container(
      constraints: const BoxConstraints(
        maxHeight: _maxHeightWidth,
      ),
      child: ListView.separated(
          // shrinkWrap: true,
          // primary: true,
          scrollDirection: Axis.horizontal,
          itemCount: mediaAttachments.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return MultiProvider(
                      providers: [
                        ChangeNotifierProvider.value(value: settingsController),
                        Provider.value(value: pathMapper)
                      ],
                      child: FacebookMediaSlideshowScreen(
                          mediaAttachments: mediaAttachments,
                          initialIndex: index));
                }));
              },
              child: FacebookMediaWrapperComponent(
                mediaAttachment: mediaAttachments[index],
                preferredWidth: isSingle ? singleWidth : preferredMultiWidth,
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(width: 10);
          }),
    );
  }
}
