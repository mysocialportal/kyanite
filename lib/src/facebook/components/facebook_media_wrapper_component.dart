import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class FacebookMediaWrapperComponent extends StatelessWidget {
  static final _logger = Logger('$FacebookMediaWrapperComponent');

  static const double _noPreferredValue = -1.0;
  final FacebookMediaAttachment mediaAttachment;
  final double preferredWidth;
  final double preferredHeight;

  const FacebookMediaWrapperComponent(
      {Key? key,
      required this.mediaAttachment,
      this.preferredWidth = _noPreferredValue,
      this.preferredHeight = _noPreferredValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingsController = Provider.of<SettingsController>(context);
    final pathMapper = Provider.of<PathMappingService>(context);
    final videoPlayerCommand = settingsController.videoPlayerCommand;
    final path = mediaAttachment.uri.scheme.startsWith('http')
        ? mediaAttachment.uri.toString()
        : pathMapper.toFullPath(mediaAttachment.uri.path);
    final width =
        preferredWidth > 0 ? preferredWidth : MediaQuery.of(context).size.width;
    final height = preferredHeight > 0
        ? preferredHeight
        : MediaQuery.of(context).size.height;

    if (mediaAttachment.estimatedType() ==
        FacebookAttachmentMediaType.unknown) {
      return Text('Unable to resolve type for ${mediaAttachment.uri.path}');
    }

    if (mediaAttachment.estimatedType() == FacebookAttachmentMediaType.video) {
      final title = "Video (click to play): " + mediaAttachment.title;
      final thumbnailImageResult = _uriToImage(
          mediaAttachment.thumbnailUri, pathMapper,
          imageTypeName: 'thumbnail image');
      if (thumbnailImageResult.image != null) {
        return _createFinalWidget(
            baseContext: context,
            imageAndPath: thumbnailImageResult,
            width: width,
            height: height,
            noImageText: 'No Thumbnail',
            noImageOnTapText:
                'Click to launch video in external player (No Thumbnail)',
            onTap: () async =>
                await _attemptToPlay(context, videoPlayerCommand, path));
      }

      return TextButton(
        onPressed: () async {
          await _attemptToPlay(context, videoPlayerCommand, path);
        },
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          Text(mediaAttachment.description)
        ]),
      );
    }

    if (mediaAttachment.estimatedType() == FacebookAttachmentMediaType.image) {
      final imageResult = _uriToImage(mediaAttachment.uri, pathMapper);
      if (imageResult.image == null) {
        final errorPath = imageResult.path.isNotEmpty
            ? imageResult.path
            : mediaAttachment.uri.toString();
        return SizedBox(
          width: width * .8,
          child: Text('Image could not be loaded: $errorPath', softWrap: true),
        );
      }

      return _createFinalWidget(
          baseContext: context,
          imageAndPath: imageResult,
          width: width,
          height: height,
          noImageText: 'No Image',
          onTap: null);
    }

    return const Text('Error creating image widget');
  }

  Future<void> _attemptToPlay(
      BuildContext context, String command, String path) async {
    _logger.fine('Attempting to launch video with $command for $path');
    try {
      await Process.run(command, [path]);
    } catch (e) {
      _logger
          .severe('Exception thrown trying to use $command to play $path: $e');
      SnackBarStatusBuilder.buildSnackbar(
          context, 'Error using $command to play video $path');
    }
  }

  _ImageAndPathResult _uriToImage(Uri uri, PathMappingService mapper,
      {String imageTypeName = 'image'}) {
    if (uri.toString().startsWith('https://interncache')) {
      return _ImageAndPathResult.none();
    }

    if (uri.scheme.startsWith('http')) {
      final networkUrl = uri.toString();
      try {
        return _ImageAndPathResult(Image.network(networkUrl), networkUrl);
      } catch (e) {
        _logger.info(
            'Error trying to create network $imageTypeName: $networkUrl. $e');
      }
      return _ImageAndPathResult.none();
    }

    if (uri.path.endsWith('mp4')) {
      return _ImageAndPathResult.none();
    }

    final fullPath = mapper.toFullPath(uri.toString());
    final imageFile = File(fullPath);
    if (imageFile.existsSync()) {
      return _ImageAndPathResult(Image.file(imageFile), fullPath);
    }

    return _ImageAndPathResult.none();
  }

  Widget _createFinalWidget(
      {required BuildContext baseContext,
      required _ImageAndPathResult imageAndPath,
      required double width,
      required double height,
      String noImageText = 'No Image',
      String noImageOnTapText = 'No Image',
      required Future<void> Function()? onTap}) {
    final noImage = imageAndPath.image == null;
    final errorText = onTap != null ? noImageOnTapText : noImageText;

    final imageWidget = noImage
        ? Text(errorText,
            style: Theme.of(baseContext)
                .textTheme
                .bodyText2
                ?.copyWith(fontWeight: FontWeight.bold))
        : SizedBox(
            width: width,
            height: height,
            child:
                Image(image: imageAndPath.image!.image, fit: BoxFit.scaleDown),
          );

    if (onTap == null) {
      return imageWidget;
    }

    return InkWell(onTap: onTap, child: imageWidget);
  }
}

class _ImageAndPathResult {
  final Image? image;
  final String path;

  _ImageAndPathResult(this.image, this.path);

  _ImageAndPathResult.none()
      : image = null,
        path = '';
}
