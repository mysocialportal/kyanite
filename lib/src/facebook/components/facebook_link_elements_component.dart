import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:metadata_fetch/metadata_fetch.dart';
import 'package:url_launcher/url_launcher.dart';

class FacebookLinkElementsComponent extends StatefulWidget {
  static final _logger = Logger('$FacebookLinkElementsComponent');
  final List<Uri> links;

  const FacebookLinkElementsComponent({Key? key, required this.links})
      : super(key: key);

  @override
  State<FacebookLinkElementsComponent> createState() =>
      _FacebookLinkElementsComponentState();
}

class _FacebookLinkElementsComponentState
    extends State<FacebookLinkElementsComponent> {
  final previewWidth = 500.0;
  final previewHeight = 165.0;
  static final _logger = Logger('$_FacebookLinkElementsComponentState');
  final _linkPreviewData = <Metadata>[];

  static Metadata _genMetadataFromUri(Uri uri) {
    final metadata = Metadata();
    metadata.url = uri.toString();
    return metadata;
  }

  @override
  void initState() {
    super.initState();
    _linkPreviewData.addAll(widget.links.map(_genMetadataFromUri));
    makeLinkPreview();
  }

  Future<void> makeLinkPreview() async {
    try {
      for (var i = 0; i < widget.links.length; i++) {
        final url = widget.links[i];
        if (!url.scheme.startsWith('http')) {
          _logger.finest('Attempted to create preview from non-HTTP url: $url');
          continue;
        }
        // Makes a call
        var response = await http.get(url);
        var document = MetadataFetch.responseToDocument(response);
        if (document == null) {
          _logger.finest(
              'Link provided for preview did not return a viable document, may be broken: $url');
          continue;
        }

        var ogData = MetadataParser.openGraph(document);
        ogData.url ??= url.toString();
        _linkPreviewData[i] = ogData;
      }

      setState(() {});
    } catch (e) {
      _logger.warning('Error getting preview for ${widget.links.first}');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.links.isEmpty) {
      return const SizedBox(height: 0, width: 0);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Links: ', style: TextStyle(fontWeight: FontWeight.bold)),
        const SizedBox(
          height: 5,
        ),
        ..._linkPreviewData.map((l) => TextButton(
            onPressed: () async {
              await canLaunch(l.url!)
                  ? await launch(l.url!)
                  : FacebookLinkElementsComponent._logger
                      .info('Failed to launch ${l.url}');
            },
            child: _buildLinkPreview(context, l))),
      ],
    );
  }

  Widget _buildLinkPreview(BuildContext context, Metadata previewData) {
    const bufferWidth = 5.0;
    const bufferHeight = 6.0;
    if ((previewData.title?.isEmpty ?? true) &&
        (previewData.description?.isEmpty ?? true) &&
        (previewData.image?.isEmpty ?? true)) {
      return Text(previewData.url ?? 'No Link Provided',
          maxLines: 5,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(fontStyle: FontStyle.italic));
    }

    return Card(
        child: SizedBox(
            width: previewWidth,
            height: previewHeight,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Image.network(previewData.image ?? '',
                        width: previewHeight,
                        height: previewHeight,
                        fit: BoxFit.cover),
                    const SizedBox(width: bufferWidth),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(previewData.title ?? '',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                          const SizedBox(height: bufferHeight),
                          Text(
                            previewData.url ?? '',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontStyle: FontStyle.italic),
                          ),
                          const SizedBox(height: bufferHeight),
                          Text(
                            previewData.description ?? '',
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          )
                        ],
                      ),
                    ),
                  ],
                ))));
  }
}
