import 'dart:io';

import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:logging/logging.dart';

import 'model_utils.dart';

enum FacebookAttachmentMediaType { unknown, image, video }

class FacebookMediaAttachment {
  static final _logger = Logger('$FacebookMediaAttachment');
  static final _graphicsExtensions = ['jpg', 'png', 'gif', 'tif'];
  static final _movieExtensions = ['avi', 'mp4', 'mpg', 'wmv'];

  final Uri uri;

  final int creationTimestamp;

  final Map<String, String> metadata;

  final List<FacebookComment> comments;

  final Uri thumbnailUri;

  final String title;

  final String description;

  FacebookMediaAttachment(
      {required this.uri,
      required this.creationTimestamp,
      required this.metadata,
      required this.thumbnailUri,
      required this.title,
      required this.description,
      required this.comments});

  FacebookMediaAttachment.randomBuilt()
      : uri = Uri.parse('http://localhost/${randomId()}'),
        creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        title = 'Random title ${randomId()}',
        thumbnailUri = Uri.parse('${randomId()}.jpg'),
        description = 'Random description ${randomId()}',
        comments = [
          FacebookComment.randomBuilt(),
          FacebookComment.randomBuilt()
        ],
        metadata = {'value1': randomId(), 'value2': randomId()};

  FacebookMediaAttachment.fromUriOnly(this.uri)
      : creationTimestamp = 0,
        thumbnailUri = Uri.file(''),
        title = '',
        description = '',
        comments = [],
        metadata = {};

  FacebookMediaAttachment.fromUriAndTime(this.uri, this.creationTimestamp)
      : thumbnailUri = Uri.file(''),
        title = '',
        description = '',
        comments = [],
        metadata = {};

  FacebookMediaAttachment.blank()
      : uri = Uri(),
        creationTimestamp = 0,
        thumbnailUri = Uri.file(''),
        title = '',
        description = '',
        comments = [],
        metadata = {};

  @override
  String toString() {
    return 'FacebookMediaAttachment{uri: $uri, creationTimestamp: $creationTimestamp, metadata: $metadata, title: $title, description: $description, comments: $comments}';
  }

  String toHumanString(PathMappingService mapper) {
    if (uri.scheme.startsWith('http')) {
      return uri.toString();
    }

    return mapper.toFullPath(uri.toString());
  }

  FacebookAttachmentMediaType estimatedType() => mediaTypeFromString(uri.path);

  FacebookMediaAttachment.fromJson(Map<String, dynamic> json)
      : uri = Uri.parse(json['uri']),
        creationTimestamp = json['creationTimestamp'],
        metadata = (json['metadata'] as Map<String, dynamic>? ?? {})
            .map((key, value) => MapEntry(key, value.toString())),
        comments = (json['comments'] as List<dynamic>? ?? [])
            .map((j) => FacebookComment.fromJson(j))
            .toList(),
        thumbnailUri = Uri.parse(json['thumbnailUri'] ?? ''),
        title = json['title'] ?? '',
        description = json['description'] ?? '';

  Map<String, dynamic> toJson() => {
        'uri': uri.toString(),
        'creationTimestamp': creationTimestamp,
        'metadata': metadata,
        'comments': comments.map((c) => c.toJson()).toList(),
        'thumbnailUri': thumbnailUri.toString(),
        'title': title,
        'description': description,
      };

  static FacebookMediaAttachment fromFacebookJson(Map<String, dynamic> json) {
    final Uri uri = Uri.parse(json['uri']);
    final int timestamp = json['creation_timestamp'] ?? 0;
    final String title = json['title'] ?? '';
    final String description = json['description'] ?? '';
    final metadata = <String, String>{};
    final thumbnailUrlString = json['thumbnail']?['uri'] ?? '';
    final thumbnailUri = thumbnailUrlString.startsWith('http')
        ? Uri.parse(thumbnailUrlString)
        : Uri.file(thumbnailUrlString);
    json['media_metadata']?.forEach((key, value) {
      if (key == 'photo_metadata' || key == 'video_metadata') {
        final exifData = value['exif_data'] ?? [];
        for (final exif in exifData) {
          exif.forEach((k2, v2) => metadata[k2] = v2.toString());
        }
      } else {
        _logger.fine("Unknown media key $key");
        metadata[key] = value;
      }
    });
    final comments = <FacebookComment>[];
    for (Map<String, dynamic> commentJson in json['comments'] ?? {}) {
      final comment = FacebookComment.fromInnerCommentJson(commentJson);
      comments.add(comment);
    }

    return FacebookMediaAttachment(
        uri: uri,
        creationTimestamp: timestamp,
        metadata: metadata,
        thumbnailUri: thumbnailUri,
        title: title,
        comments: comments,
        description: description);
  }

  static FacebookAttachmentMediaType mediaTypeFromString(String path) {
    final separator = Platform.isWindows ? '\\' : '/';
    final lastSlash = path.lastIndexOf(separator) + 1;
    final filename = path.substring(lastSlash);
    final lastPeriod = filename.lastIndexOf('.') + 1;
    if (lastPeriod == 0) {
      return FacebookAttachmentMediaType.unknown;
    }

    final extension = filename.substring(lastPeriod).toLowerCase();

    if (_graphicsExtensions.contains(extension)) {
      return FacebookAttachmentMediaType.image;
    }

    if (_movieExtensions.contains(extension)) {
      return FacebookAttachmentMediaType.video;
    }

    return FacebookAttachmentMediaType.unknown;
  }
}
