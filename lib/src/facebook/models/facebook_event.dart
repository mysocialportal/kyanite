import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/models/facebook_location_data.dart';
import 'package:logging/logging.dart';

import 'model_utils.dart';

enum FacebookEventStatus {
  declined,
  interested,
  invited,
  joined,
  owner,
  unknown,
}

class FacebookEvent {
  static final _logger = Logger('$FacebookEvent');

  final String name;
  final String description;
  final int creationTimestamp;
  final int startTimestamp;
  final int endTimestamp;
  final FacebookLocationData location;
  final FacebookEventStatus eventStatus;

  FacebookEvent(
      {required this.name,
      required this.description,
      required this.creationTimestamp,
      required this.startTimestamp,
      required this.endTimestamp,
      required this.location,
      required this.eventStatus});

  @override
  String toString() {
    return 'FacebookEvent{name: $name, description: $description, creationTimestamp: $creationTimestamp, startTimestamp: $startTimestamp, endTimestamp: $endTimestamp, location: $location, eventStatus: $eventStatus}';
  }

  String toHumanString(DateFormat formatter) {
    final creationDateString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(creationTimestamp * 1000)
            .toLocal());
    final startTimeString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(startTimestamp * 1000).toLocal());
    final endTimeString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(endTimestamp * 1000).toLocal());
    return [
      if (name.isNotEmpty) 'Name: $name',
      if (description.isNotEmpty) 'Description:\n$description',
      'Creation At: $creationDateString',
      if (startTimestamp != 0) 'Start Time: $startTimeString',
      if (endTimestamp != 0) 'End Time: $endTimeString',
      'Your Status: $eventStatus',
      if (location.hasPosition) location.toHumanString(),
    ].join('\n');
  }

  static FacebookEvent fromJson(Map<String, dynamic> json,
      {FacebookEventStatus statusType = FacebookEventStatus.unknown}) {
    final knownTopLevelKeys = [
      'name',
      'start_timestamp',
      'end_timestamp',
      'place',
      'description',
      'create_timestamp'
    ];

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level event keys');

    final name = json['name'] ?? '';
    final description = json['description'] ?? '';
    final int creationTimestamp = json['create_timestamp'] ?? 0;
    final int startTimestamp = json['start_timestamp'] ?? 0;
    final int endTimestamp = json['end_timestamp'] ?? 0;
    final FacebookLocationData location = json.containsKey('place')
        ? FacebookLocationData.fromJson(json['place'])
        : const FacebookLocationData();

    return FacebookEvent(
        name: name,
        description: description,
        creationTimestamp: creationTimestamp,
        startTimestamp: startTimestamp,
        endTimestamp: endTimestamp,
        location: location,
        eventStatus: statusType);
  }
}
