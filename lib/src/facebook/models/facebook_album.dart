import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:logging/logging.dart';

import 'model_utils.dart';

class FacebookAlbum {
  static final _logger = Logger('$FacebookAlbum');

  final String name;
  final String description;
  final int lastModifiedTimestamp;
  final FacebookMediaAttachment coverPhoto;
  final List<FacebookMediaAttachment> photos;
  final List<FacebookComment> comments;

  FacebookAlbum(
      {required this.name,
      required this.description,
      required this.lastModifiedTimestamp,
      required this.coverPhoto,
      required this.photos,
      required this.comments});

  static FacebookAlbum fromJson(Map<String, dynamic> json) {
    final knownAlbumKeys = [
      'name',
      'photos',
      'cover_photo',
      'last_modified_timestamp',
      'comments',
      'description'
    ];

    logAdditionalKeys(knownAlbumKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level album keys');

    String name = json['name'] ?? '';
    String description = json['description'] ?? '';
    int lastModifiedTimestamp = json['last_modified_timestamp'] ?? 0;
    FacebookMediaAttachment coverPhoto = json.containsKey('cover_photo')
        ? FacebookMediaAttachment.fromFacebookJson(json['cover_photo'])
        : FacebookMediaAttachment.blank();

    final photos = <FacebookMediaAttachment>[];
    for (Map<String, dynamic> photoJson in json['photos'] ?? []) {
      photos.add(FacebookMediaAttachment.fromFacebookJson(photoJson));
    }

    final comments = <FacebookComment>[];
    for (Map<String, dynamic> commentsJson in json['comments'] ?? []) {
      comments.add(FacebookComment.fromInnerCommentJson(commentsJson));
    }

    return FacebookAlbum(
        name: name,
        description: description,
        lastModifiedTimestamp: lastModifiedTimestamp,
        coverPhoto: coverPhoto,
        photos: photos,
        comments: comments);
  }
}
