# Kyanite Changelog

## Version 1.0.0

First release version of the application

## Bug Fixes
* Fix links not showing when network not available or error getting link
* Fix the stat panel updates not propagating correctly when type or filter changed


## Version 0.1.3 (2021-12-20)

### New Features
* Link previews for posts that have resolvable URLs with OpenGraph data
* Saved items screen to show that data, mostly external links 
* New statistics panel
  * Select between posts, comments, photos, and videos 
  * Display graph of posts by day of week, month of year, or year
  * Display heat map for a selected year of posts per day
  * Display list of the top 10, 20, 50, or 100 posts (user selectable)
  * Support same filtering as post, comment, photo, and video panels
### Bug Fixes
* Filter panel
  * Text field with start/stop dates read only to reflect not being editable
### Changes
* Navigation bar is scrollable for smaller window sizes
* Allow direct editing of dates in the calendar control
* Scrollbars are disabled on posts, comments, etc. 
* Scrolling performance improved by deferring loading of quickly passed rows


## Version 0.1.2 (2021-12-07)
### New Features
* Make Photo Details an image carousel on posts/albums with multiple images
* Let users navigate photo details carousel with arrow keys and go back to former screen with escape-key
* Added a "copy" button on posts, comments, conversations that copies all the textual data to the clipboard
* Adds a map view for posts/photos that have latitude/longitude data

### Bug Fixes
* Fixes memory leak with images and posts
* Fixes error where default video player was set to empty string on initial startup
* Fix capitalization inconsistencies on buttons

### Changes
* Change log file textbox on settings panel to be single line and overflow with ellipses 


## Version 0.1.1 (2021-11-17)

### Bug Fixes
* Add support for update Facebook archive format (versus original one from a year ago)

## Version 0.1.0 (2021-11-14) ** [Initial Release] **
### New Features
* Posts Browsing/filtering (including media and links)
* Comments Browsing/filtering (including media and links)
* Photo Albums Browsing/filtering  (and photos attached to posts and comments)
* Video Album Browsing/filtering (and videos attached to posts and comments)
* Facebook Messenger Conversation Browsing/filtering  (with media and links)
* Events Browsing/filtering 
* Friends list and history browsing
* Ability to export photos from posts/comments/albums/etc.
